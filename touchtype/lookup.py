from __future__ import absolute_import

import random
import time
from collections import Counter

from nltk.corpus import words
from pattern.en import verbs


FILE_NAME = 'touchtype/data/combined'

FREQUENCY_FILES = (
    'touchtype/data/brown',
    'touchtype/data/gutenberg',
)


def create_frequency_dictionary():
    c = Counter()
    for file_name in FREQUENCY_FILES:
        with open(file_name) as f:
            lines = f.readlines()
            for line in lines:
                line = line.strip().lower()
                c[line] += 1
    return c


def create_word_dictionary():
    words = {}
    with open(FILE_NAME) as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip()
            # Add word
            if line.lower() not in words:
                # Don't add in the capitalized version
                words[line.lower()] = set()
            words[line.lower()].add(line)

            # Add conjugated verbs
            line_verbs = verbs.get(line.lower(), [])
            for vline in line_verbs:
                vline = str(vline.lower())
                if vline not in words:
                    words[vline] = set()
                words[vline].add(vline)

    return words


def benchmark(n=1000):
    """Helper function to benchmark large scale dictionary lookup."""
    words = create_word_dictionary()
    times = []
    with open(FILE_NAME) as f:
        lines = f.readlines()
        lines = [line.strip().lower() for line in lines]

        total = len(lines) - 1

        for i in range(n):
            idx = random.randint(0, total)
            t_a = time.time()
            found = lines[idx] in words
            times.append(time.time() - t_a)

    print 'avg time:', sum(times) / len(times)
    print 'total time:', sum(times)
