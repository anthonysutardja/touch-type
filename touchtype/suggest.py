from __future__ import absolute_import

import itertools

from touchtype.lookup import (
    create_word_dictionary,
    create_frequency_dictionary,
)


# Contains english language and male/female names
WORDS = create_word_dictionary()
FREQUENCY = create_frequency_dictionary()


def find(ch_mapping, characters):
    """Return a list of suggested words that match the set of characters."""
    # NOTE: mapping needs to be strings
    ch_args = []
    for ch in characters:
        ch_args.append(ch_mapping[ch])
    possible_words = [''.join(word) for word in itertools.product(*ch_args)]
    suggestions = []
    for word in possible_words:
        if is_word(word):
            suggestions.extend(list(WORDS[word]))
    suggestions.sort(key=lambda word: FREQUENCY[word], reverse=True)
    return suggestions


def is_word(word):
    """Return whether or not the word exists in the English dictionary."""
    return word.lower() in WORDS
