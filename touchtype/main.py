# coding=utf-8
from __future__ import absolute_import

import serial
import time

from touchtype.suggest import find
from touchtype.profiles import ANTHONY_PROFILE


ser = serial.Serial('/dev/tty.usbmodemfa132', 9600)
character_map = {}
encountered_characters = set()

# character_map = ANTHONY_PROFILE
character_map = {}
TRAIN_TEMPLATE = 'the quick brown fox jumps over the lazy dog'


def get_word(suggestions, selection):
    if suggestions:
        idx = (selection + len(suggestions)) % len(suggestions)
        return suggestions[idx]
    return ''


def train_loop():
    # Training phase
    print '========================================'
    print 'Training..'
    print '(Do not use any punctuation or capitalization)'
    tokens = []
    while len(tokens) != len(TRAIN_TEMPLATE):
        token = ser.read()
        print 'Token:', token
        tokens.append(token)

    for idx, token in enumerate(tokens):
        if token not in character_map:
            character_map[token] = set()
        if TRAIN_TEMPLATE[idx] != ' ':
            character_map[token].add(TRAIN_TEMPLATE[idx])

    for token, characters in character_map.items():
        character_map[token] = ''.join(characters)

    # Make 4 and 5 reserved space characters
    character_map['4'] = ' '
    character_map['5'] = ' '

    print 'Character map:', character_map
    print 'Done training!'
    print '========================================'
    print '\n'


def loop():
    # Ready to type phase
    print '========================================'
    print 'Ready for text input..'
    tokens = []
    sentence = ''
    suggestions = []
    selection = 0

    while True:
        token = ser.read()
        print 'Key entered:', token
        # TODO:
        #       - add superlatives
        #       - add deletion

        if token == 'a':
            selection -= 1
        elif token == 'b':
            selection += 1
        elif character_map[token] == ' ':
            if suggestions:
                # commit the selected word
                sentence += get_word(suggestions, selection) + ' '
            else:
                # if sentence and sentence[-1] != ' ':
                # if double space then remove previous word
                sentence += ' '
            # Reset
            tokens = []
            suggestions = []
            selection = 0
        else:
            tokens.append(token)
            suggestions = find(character_map, tokens)

        print "Selection:", selection, get_word(suggestions, selection)
        print 'Suggestions:', suggestions
        print sentence + ' ' + ''.join(['█' for i in range(len(tokens))])
        print '----------------------------------------'


if __name__ == '__main__':
    train_loop()
    loop()
