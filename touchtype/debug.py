from __future__ import absolute_import

import serial
import time

ser = serial.Serial('/dev/tty.usbmodemfa132', 9600)


def loop():
    while True:
        print ser.read()


if __name__ == '__main__':
    loop()
